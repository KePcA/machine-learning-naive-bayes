#!/usr/bin/python
# -*- coding: iso-8859-2 -*-

'''
Created on 21. dec. 2013

@author: Pavilion
'''




"""

NEUPORABLJENE KRATICE:
HISTRION-LAG : 1
MOR : 1
CO : 9
R : 7 - lahko pomeni rezervni ali ra�un
S : 1
NAL: 1
�L : 1 - �LANSTVO
POL : 1 - POLO�NICA
LET : 1 - LETNA
KUR : 1 - KURILNA
BOH : 4 - BOHINJSKA
L : 9 - LETO
V : 6 - VODNO
K : 1 - KARTICA
LEK : 1 - LEKARNA
�TUD : 1 - �TUDENTSKO
AVT : 1 - 
IZD : 4 - IZDAJA
NEZG : 2 - NEZGODNO
OPR : 1 - 
KRAT : 1 - KARTICA
NAJEMN : 1 - NAJEMNINA
DIJ : 1 - DIJAK
GASP : 1 - 
UPR : 1 - UPRAVLJANJE
TEK : 1 - 
J : 1 - NEK PRIIMEK
PRIPEV : 1 - PRISPEVEK
BK : 1 - 
JULI : 1 - JULIJ
NO : 1 - NUMBER
OKR : 3 - 
ODD: 9 - 
SI: 2 - SIMOBIL
ODLP : 1 - ODPLA�ILO
M : 1 - BREZ POMENA
REG : 1 - REGISTRACIJA
MES : 1 - MESE�NI
DOP : 1 - DOPOLNILNO
POSTOP : 1 - POSTOPNO
OS : 1 - 
SK : 2 - SKLAD in �KOFJA LOKA
D : 11 - ?
MERC : 2 - MERCATOR
SPEC. : 1 -
STAR : 1 - STAR�EVSKO
IND : 1 - 
DOD : 1 - DODATEK
PREK : 1 - 
ORG : 1 - 
B : 2 -
DIGIT : 1 - DIGITALNO
C : 1 -
ZAVAROVAL : 1 - ZAVAROVALNICA
UL : 1 - ULICA
E : 1 - E LECLERC
OBVEZN : 1 - OBVEZNOST
ODST : 1 - ODSTOTEK
POVRAC : 1 - POVRA�ILO
DOD : 2 - DODIPLOMSKI, DODATNI
KOL : 1 - KOLEKTIVNI
KO : 1 -
TOPLO : 1 - 
ZELEZ : 1 - ZELEZNISKA
GR : 1 - 
NADOM : 1 - NADOMESTILO
FLEX : 1 - 
POSAMEZ : 1 - 
IZVED : 1 - 
AMBUL : 1 - AMBULANTA
SPAGET : 1 - SPAGETARIJA
SLOV : 3 - SLOVENSKA
PREH : 1 - 
DRAG : 1 - 
DODAT : 1 - DODATEK
ZADR : 2 - ZADRUGA
OTR : 1 - 
NETO-ODB : 1 - 
POSKOD : 1 - POSKODBA
A : 1 - NEK PRIIMEK
� : 2 - �TUDIJSKI
POSTONJ : 1 - POSTONJSKA
ZAHT : 1 - ZAHTEVEK
POTOV : 1 - POTOVANJE
SV : 1 - 
BLAG : 1 - 
PON : 2 - 
TEHN : 1 - TEHNI�NI
KABEL : 1 - KABELSKA
P. : 1 - PREMIJA
STVB : 1 - STAVBNO
ZEMLJ : 1 - ZEMLJI��E
DOKUM : 1 - DOKUMENTACIJA
NAP : 1 - 
ROM : 1 - 
POST : 1 - POSTOPNO
KUPOPROD : 1 - KUPOPRODAJNA
NOTR : 1 - NOTRANJI
UK : 1 - UKINITEV
MOB : 1 - MOBITEL
TEL : 1 - TELEFON
U�B : 1 -U�BENIK
GIM : 1 - GIMNATIJA
O : 1 - OBVEZNOST

Iz druge datoteke (pomen posebnih besed)
GR : 12 - GRADEC



Nagaja lahko
-OBV - lahko pomeni OBVEZNOST ali OBVEZNO
-ENERG - lahko pomeni ENERGIJA ali ENERGETSKI
-VOD - pomen je VODARINA ampak nekje pi�ejo VODA
-�T - pomen je lahko �TEVILKA ali STORITEV
-OBR - pomen je lahko OBRA�UN ali OBRESTI
-TRG - pomen TRG ali TRGOVINA





NEUPORABLJENE POSEBNE BESEDE:
R.D. : 1
K.S. : 2
T.Z. : 1
M.A.C. : 1
I.P. : 1
S.P.M. : 1
T.C. : 1



Nagaja lahko:

"""



from unidecode import unidecode;
import codecs;
import preprocessing_user_inputs;
import preprocess;







abbr_dict = [];
spec_word_dict = [];


data = codecs.open('kategorizirani_podatki_2.txt', encoding="utf-8");


#TABELO PODATKOV - (UP. VNOS , KATEGORIJA)
data.readline();
data.readline();
transactions_data = preprocessing_user_inputs.get_data_in_table_with_category_2(data);

data.close();

def has_abbr(abbr, spec_dict):
    for item in spec_dict:
        current_abbr = item[0]
        if abbr == current_abbr:
            return True;
    return False;


for transaction in transactions_data:
    user_input = transaction[0];
    user_input = preprocess.tokenize_tokens(user_input);    #RAZBIJEMO NA BESEDE
    #user_input = preprocess.filter_tokens(user_input, special_chars=True); #ODSTRANIMO POSEBNE ZNAKE
    user_input = preprocessing_user_inputs.remove_slo_stoppwords(user_input); # ODSTRANIMO NEPOMEMBNE SLOVENSKE BESEDE
    
    for token in user_input:
        if token[-1] == '.':       #JE KRATICA
            
            if not has_abbr(token, abbr_dict): #KRATICE NI NOTRI, JO DODAMO
                input_counter = {transaction[0]: 1};
                abbr_dict.append([token, input_counter]);
                
            else: #KRATICA JE NOTRI, JO NAJDEMO IN DODAMO VNOS
                
                for index in range(len(abbr_dict)):
                    if token == abbr_dict[index][0]:  #ČE SMO NALI KRATICO KRATICA E NOTRI
                        input_counter = abbr_dict[index][1];  #SLOVAR, KI TEJE POJAVITVE VNOSOV
                        if input_counter.has_key(transaction[0]) :  #ČE SE JE VNOS žE POJAVIL POVEČAMO TEVEC
                            input_counter[transaction[0]] += 1;
                        else:                                   #VNOS SE šE NI POJAVIL, GA VNESEMO PRVIČ
                            input_counter[transaction[0]] = 1;
            
    
    user_input = preprocess.filter_tokens(user_input, special_chars=True); #ODSTRANIMO POSEBNE ZNAKE (TUDI KRATICE)
    
    for token in user_input:
        if len(token)<=5:       #JE MOGO�E POSEBNA BESEDA
            
            if not has_abbr(token, spec_word_dict): #POSEBNE BESEDE NI NOTRI, JO DODAMO
                input_counter = {transaction[0]: 1};
                spec_word_dict.append([token, input_counter]);
                
            else: #POSEBNA BESEDA JE NOTRI, JO NAJDEMO IN DODAMO VNOS
                
                for index in range(len(spec_word_dict)):
                    if token == spec_word_dict[index][0]:  #ȌE SMO NA��LI POSEBNO BESEDO �E NOTRI
                        input_counter = spec_word_dict[index][1];  #SLOVAR, KI �TEJE POJAVITVE VNOSOV
                        if input_counter.has_key(transaction[0]) :  #ȌE SE JE VNOS �E POJAVIL POVE�AMO ��TEVEC
                            input_counter[transaction[0]] += 1;
                        else:                                   #VNOS SE šE NI POJAVIL, GA VNESEMO PRVIČ
                            input_counter[transaction[0]] = 1;




#PIE�MO V DATOTEKO ZA KRATICE
output = codecs.open('analiza_kratic.txt', 'w', encoding="utf-8");

for item in abbr_dict:
    abbr = item[0];
    input_counter = item[1];
    output.write(abbr);
    output.write('\n');
    for key in input_counter:
        output.write(key);
        output.write(':  ')
        output.write(str(input_counter[key]));
        output.write('\n');
    output.write('\n');
    output.write('\n');
    
output.close();





#PI�EMO V DATOTEKO ZA POSEBNE BESEDE
output = codecs.open('analiza_posebnih_besed.txt', 'w', encoding="utf-8");

for item in spec_word_dict:
    special_word = item[0];
    input_counter = item[1];
    output.write(special_word);
    output.write('\n');
    for key in input_counter:
        output.write(key);
        output.write(':  ')
        output.write(str(input_counter[key]));
        output.write('\n');
    output.write('\n');
    output.write('\n');
    
output.close();



    
    