#!/usr/bin/python
# -*- coding: iso-8859-2 -*-

'''
Created on 7. dec. 2013

PREDPROCESIRANJE TEXTA - UPORABNISKIH VNOSOV.

@author: Igor Klepi�
'''



"""
TE�AVE:
    -lematizacije - pla�ilo, nakazilo (utf encoding, dodaj pravilo za nakazilo)
    -izbris �udnih vnosov - prazen vnos - posebna kategorija
    -odstanitev posebnih znakov - vnos ppstane neregularen npr. (RACUN-3332) ko odstranimo - nadomestimo s presledkom
    -kratice z eno �rko - odstrani npr, D.O.O., D.D.
    - zamenjava 蚞 - unidecode knji�nica
    
    -posebni izrazi npr. BS -slovar
    -samo �tevilke brez pik pustimo, zamenjamo zz skupno besedo
    -odstrani imena ali pa vsa vsa imena zamenjaj z enakim nizom - knji�nica imen DONE!
    -open file - z razredom codecs
    
    
    TODO: 18.12.2013:
        -lematizacija, ponovno se nau�iti
        -spraviti v obliko (vnos, kategorija)
        -prazne vnose posebna kategorija
        -napolni seznam posebnih vnosov npr d.o.o., bs
        -unidecode uporabi povsod
        -napolni podatke z imeni
        -蚞 je popravi, tudi v slovarju
    
"""

# -*- coding: utf8 -*-
import preprocess;
import re;
from unidecode import unidecode;
import codecs;







""" 
__________KRATICE_______________

"""
#VRNE POMEN KRATICE, �E JO NAJDE V SLOVARJU, SICER VRNE ORIGINALNO BESEDO
def replace_abbr(abbr, abbr_dict):
    if abbr_dict.has_key(abbr):
        return abbr_dict[abbr];
    else:
        return abbr
    
#SPREJME VNOS RAZBIT NA BESEDE IN ZAMENJA MOREBITNE KRATICE
def replace_all_abbr(tokens, abbr_dict):
    result = [];
    for token in tokens:
        token = replace_abbr(token, abbr_dict);
        result.append(token);
    return result;






"""
________________POSEBNE BESEDE_____________

"""

#VRNE POMEN POSEBNE BESEDE RAZBIZE NA POSAMEZNE BESEDE, �E JO NAJDE V SLOVARJU, SICER VRNE ORIGINALNO BESEDO V TABELI
def replace_special_word_with_meaning(spec_word, spec_words_dict):
    if spec_words_dict.has_key(spec_word):
        return spec_words_dict[spec_word].split("_");
    else:
        return [spec_word];


#SPREJME VNOS RAZBIT NA BESEDE IN ZAMENJA MOREBITNE POSEBNE BESEDE S CELIMI
def replace_all_spec_words(tokens, spec_words_dict):
    result = [];
    for token in tokens:
        result += replace_special_word_with_meaning(token, spec_words_dict);
    return result;






"""
________________________POSEBNI ZNAKI____________

"""

#ZAMENJA POSEBNE ZNAKE Z PRESLEDKOM
def replace_special_characters_with_space(text_input):
    special_char = [":", ";", "/", "-", "."];
    for char in special_char:
        text_input = text_input.replace(char, " ");
    return text_input;

#SPREJME VNOS RAZBIT NA BESEDE IN ZAMENJA POSEBNE ZNAKE S PRESLEDKI (KLI�E ZGORNJO METODO
def replace_spec_char(tokens):
    result = []
    for token in tokens:
        token = replace_special_characters_with_space(token);
        token = token.strip();
        token = token.split(" ");
        result += token;
    return result;


#ZAMENJA �UMNIKE IN SE�NIKE Z NAVADNIMI �RKAMI V NIZU
def replace_slovenian_characters_2(word):
    characters = ['�','�','�','�','�','�','�','�','�','�'];
    word = word.replace('�','C');
    word = word.replace('�','C');
    word = word.replace('�','S');
    word = word.replace('�','S');
    word = word.replace('�','Z');
    word = word.replace('�','Z');
    word = word.replace('�','DJ');
    word = word.replace('�','DJ');
    word = word.replace('�','C');
    word = word.replace('�','C');
    return word;      




"""
________________________IMENA_______________

"""

#PREVERI, �E JE BESEDA IME - POGLEDA V TABELO IMEN
def replace_name(token, names_dict):
    if token in names_dict:
        return "ABCDEFGH";
    else:
        return token;
    
#SPREJMEN VNOS RAZBIT NA BESEDE IN MOREBITNA IMENA ZAMENJA Z ISTIM STRINGOM
def replace_all_names(tokens, names_dict):
    result = [];
    for token in tokens:
        token = replace_name(token, names_dict)
        result.append(token);
    return result;

#IZ DATOTEKE NAPOLNI TABELO IMEN
def fill_names_from_file(file):
    names_dict = [];
    data = codecs.open(file, encoding="utf-8");
    for line in data.readlines():
        names_dict.append(line.strip());
    data.close();
    return names_dict;













#IZ DATOTEKE PREBERE IN NAPOLNI SLOVAR
def fill_dictionary(file):
    dict = {};
    data = codecs.open(file, encoding="utf-8");
    for line in data.readlines():
        if line.strip() == '':
            continue;
        item = line.split(" ");
        dict[item[0].strip()] = item[1].strip();
    data.close();
    return dict;





"""

__________________________PREDPROCESIRANJE TEXTA_____________________________
"""

#PROCESIRA UPORABNI�KI VNOS
def preprocess_user_input_2(user_input, spec_word_dict, abbr_dict, names_dict):
    user_input = user_input.upper(); #PRETVORIMO VSE V VELIKE �RKE
    user_input = replace_slovenian_characters_2(user_input); #ODSTRANIMO Ȋ� JE ZAMENJAMO Z CSZ
    user_input = preprocess.tokenize_tokens(user_input);    #RAZBIJEMO NA BESEDE
    user_input = replace_all_spec_words(user_input, spec_words_dict); #POSEBNE BESEDE ZAMENJAMO S POMENOM
    user_input = replace_spec_char(user_input) #ODSTRANIMO POSEBNE ZNAKE S PRESLEDKI
    user_input = replace_all_spec_words(user_input, spec_words_dict); #POSEBNE BESEDE ZAMENJAMO S POMENOM �E ENKRAT
    user_input = remove_slo_stoppwords(user_input); # ODSTRANIMO NEPOMEMBNE SLOVENSKE BESEDE
    user_input = replace_all_abbr(user_input, abbr_dict); #ZAMENJAMO VSE MOREBITNE KRATICE
    user_input = replace_all_names(user_input, names_dict);
    user_input = preprocess.filter_tokens(user_input, min_size=2, special_chars=True); #ODSTRANIMO POSEBNE ZNAKE IN STRINGE DOL�INE 1
    
    return user_input;

#ODSTRANI NEPOMEMBNE SLOVENSKE BESEDE
def remove_slo_stoppwords(tokens):
    result = [];
    slo_stoppwords = ['IN', 'CE', 'KER', 'DO', 'PRED', 'PO', 'ZA', 'SKOZI', 'KI', 'SE', 'NA', 'PRI'];
    for token in tokens:
        if token in slo_stoppwords:
            continue;
        result.append(token);
    return result;
              



#VRNE TABELO PODATKOV V OBLIKI (upor.vnos, kategorija)
def get_data_in_table_with_category_2(data):
    transactions = [];
    for line in data:
        line = line.strip();
        category = line.split(" ")[0];
        input = line.replace(category, '', 1).strip();
        transactions.append((input,category));       
    return transactions;

#PREDPROCESIRA TEXT - DOBIMO TABELO OBLIKE (upor_vnos=(beseda1, beseda2,...,besedan) kategorija)
def preprocess_data_2(file, spec_word_dict={}, abbr_dict={}, names_dict=[]):
    data = codecs.open(file, encoding="utf-8");     #ODPREMO DATOTEKO
    data.readline();            #SPUSTIMO DVE VRSTI                    
    data.readline();
    transactions = get_data_in_table_with_category_2(data);   #PRIDOBIMO TABELO PODATKOV OBLIKE [(upor. vnos) kategorija]
    data.close();
    transactions = preprocess_transactions_data_2(transactions, spec_word_dict, abbr_dict, names_dict);
    return transactions;


#ZA VSAKO TRANSAKCIJO KLI�E ZGORNJO METODO, KI PROCESIRA VNOS IN VRNE TABELO VSEH S PRIPADAJO�O KATEGORIJO
def preprocess_transactions_data_2(transactions, spec_word_dict, abbr_dict, names_dict):
    preprocessed_transactions = [];
    for transaction in transactions:
        preprocessed_user_input = preprocess_user_input_2(transaction[0], spec_word_dict, abbr_dict, names_dict);
        category = transaction[1];
        preprocessed_transactions.append((preprocessed_user_input, category));
    return preprocessed_transactions;


#IZ TABELE TRANSAKCIJ PO PROCESIRANJU ODSTRANI TISTE, KI SO PRAZNE
def remove_empty_ones(transactions):
    result = [];
    for item in transactions:
        if len(item[0] == 0):
            continue;
        result.append(item);
    return result;


#VRNE TABELO TESTNIH PODATKOV. IZ TABELE KI IMA ZA ELEMENTE (upor_vnos= [vnos1,vnos2,...,vnosn],kategorija)
#VRNE RAZBITE VNOSE V OBLIKI (vnos1, kategorija) (vnos2, kategorija) ... 
def get_test_data_2(transactions):
    test_data = [];
    for transaction in transactions:
        category = transaction[1];
        for input in transaction[0]:
            test_data.append((input, category));
    return test_data;



"""
________________________________LEMATIZACIJA________________________________________

"""

#ZAPI�E PREDPROCESIRANE PODATKE V DATOTEKO ZA LEMATIZACIJO
def create_file_for_lemmatization_2(transactions, file_path):
    file = codecs.open(file_path, 'w', encoding="utf-8");
    for transaction in transactions:
        for token in transaction[0]:
            file.write(token.lower());
            file.write(" ");
        file.write(transaction[1]);
        file.write('\n');
    file.close();


#BERE LEMATIZIRANE PODATKE V TABELO
def read_from_file_after_lemmatization_2(file_path):
    transactions = [];
    file = codecs.open(file_path, 'r', encoding="utf-8")
    for line in file:
        tokens = line.split(" ");
        for index in range(len(tokens[:-1])):
            tokens[index] = tokens[index].upper();         
        transaction = (tokens[:-1], tokens[-1].strip());
        transactions.append(transaction);
    return transactions;





#NAPOLNIMO SLOVAR
abbr_dict = fill_dictionary('pomen_kratic.txt');
spec_words_dict = fill_dictionary('pomen_posebnih_besed.txt');
names_dict = fill_names_from_file('imena.txt');



#PRIDOBIMO TRANSAKCIJE
#transactions = preprocess_data_2('kategorizirani_podatki_2.txt', spec_words_dict, abbr_dict, names_dict);

#print transactions

#PREPI�EMO PODATKE V DATOTEKO ZA LEMATIZACIJO
#create_file_for_lemmatization_2(transactions, './Lemmatize/lemmagen/binary/win32/release/before_lematize.txt');

print "JULIJ MAREC 10".split();