#!/usr/bin/python
# -*- coding: iso-8859-2 -*-

'''
Created on 7. dec. 2013

PREDPROCESIRANJE TEXTA - UPORABNISKIH VNOSOV.

@author: Igor Klepi�
'''



"""
TE�AVE:
    -lematizacije - pla�ilo, nakazilo (utf encoding, dodaj pravilo za nakazilo)
    -izbris �udnih vnosov - prazen vnos - posebna kategorija
    -odstanitev posebnih znakov - vnos ppstane neregularen npr. (RACUN-3332) ko odstranimo - nadomestimo s presledkom
    -kratice z eno �rko - odstrani npr, D.O.O., D.D.
    - zamenjava 蹾 - unidecode knji�nica
    
    -posebni izrazi npr. BS -slovar
    -samo �tevilke brez pik pustimo, zamenjamo zz skupno besedo
    -odstrani imena ali pa vsa vsa imena zamenjaj z enakim nizom - knji�nica imen DONE!
    -open file - z razredom codecs
    
    
    TODO: 18.12.2013:
        -lematizacija, ponovno se nau�iti
        -spraviti v obliko (vnos, kategorija)
        -prazne vnose posebna kategorija
        -napolni seznam posebnih vnosov npr d.o.o., bs
        -unidecode uporabi povsod
        -napolni podatke z imeni
        -蹾 je popravi, tudi v slovarju
    
"""

# -*- coding: utf8 -*-
import preprocess;
import re;
from unidecode import unidecode;
import codecs;

#VRNE TABELO PODATKOV V OBLIKI (upor.vnos, kategorija)
def get_data_in_table_with_category(data):
    transactions = [];
    for line in data:
        line = line.strip();
        category = line.split(" ")[0];
        input = line.replace(category, '', 1).strip();
        transactions.append((input,category));       
    return transactions;


#IZ DATOTEKE NAPOLNI SLOVAR S KRATICAMI
def fill_abbreviation_dictionary(file):
    abbr_dict = {};
    data = open(file,'r');
    for line in data:
        if line == '\n':
            continue;       
        item = line.split(" ");
        abbr_dict[item[0].strip()] = item[1].strip();
    data.close();
    return abbr_dict;


#PREVERI, CE JE KRATICA
def is_abbreviation(string):
    return string[-1] == '.';

#ODSTRANI POSEBNE ZNAKE V NIZU
def remove_special_characters(tokens):
    special_char = ['.', ':', ';', '|', '-',''];
    for token in tokens:
        if token in special_char:
            tokens.remove(token);
    return tokens;




#ZAMENJA �UMNIKE IN SE�NIKE Z NAVADNIMI �RKAMI V NIZU
def replace_slovenian_characters(word):
    characters = ['�','�','�','�','�','�','�','�','�','�'];
    word = word.replace('�','C');
    word = word.replace('�','C');
    word = word.replace('�','S');
    word = word.replace('�','S');
    word = word.replace('�','Z');
    word = word.replace('�','Z');
    word = word.replace('�','DJ');
    word = word.replace('�','DJ');
    word = word.replace('�','C');
    word = word.replace('�','C');
    return word;


#PREVERI, �E GRE ZA NEPOMEMBNO BESEDO - VEZNIK, PREDLOG IPD.
def is_slovenian_stopword(word):
    stopwords = ['IN', 'CE', 'KER', 'DO', 'PRED', 'PO', 'ZA', 'SKOZI', 'KI', 'SE', 'NA', 'PRI']
    return word in stopwords;

#POI��E POMEN KRATICE. �E POMENA NI, VRNE NAZAJ KRATICO
def replace_abbreviation(abbreviation):
    if abbr_dict.has_key(abbreviation):
        return abbr_dict[abbreviation];
    else:
        return abbreviation

#V SEZNAMU NIZOV ZAMENJA VSE KRATICE, KI JIH IMA V SEZNAMU
def replace_all_abbreviations(abbr_list):
    result = [];
    for abbr in abbr_list:
        replaced_abbr = replace_abbreviation(abbr);
        result.append(replaced_abbr);
    return result;
        

#PREDPROCESIRAMO UPORABNISKI VNOS: RAZBIJEMO NA BESEDE, ODSTRANIMO POSEBNE ZNAKE, RAZ�IIMO KRATICE
def preprocess_user_input(user_input):
    result = [];
    tokens = preprocess.tokenize_tokens(user_input);
    tokens = remove_special_characters(tokens);
    
    for token in tokens:
        token = replace_slovenian_characters(token);
        token = token.upper();
        #�E JE NEPOMEMBNA BESEDA, JO SPUSTIMO
        if is_slovenian_stopword(token):
            continue;
        if is_abbreviation(token):                      #GRE ZA KRATICO
            token = token[:-1];                             #ODSTRANIMO PIKO
        oth_abbr = re.split(r'[.;\-:]+', token);    #RAZBIJEMO NA OSTALE KRATICE Z REGULARNIM IZRAZOM
        oth_abbr = replace_all_abbreviations(oth_abbr);  #VSE KRATICE ZAMENJAMO S CELIMI BESEDAMI
        result = result + oth_abbr;
    result = preprocess.filter_tokens(result, special_chars=True);  #ODSTANIMO POSEBNE ZNAKE
    result = preprocess.filter_tokens(result, min_size=2); #ODSTRANIMO ZNAKE DOL�INE 1
    result = remove_special_characters(result); #ODSTRANIMO OSTALE ZNAKE, KI SE SE POJAVIJO
    return result;





#ZA VSAKO TRANSAKCIJO KLI�E ZGORNJO METODO, KI PROCESIRA VNOS IN VRNE TABELO VSEH S PRIPADAJO�O KATEGORIJO
def preprocess_transactions_data(transactions):
    preprocessed_transactions = [];
    for transaction in transactions:
        preprocessed_user_input = preprocess_user_input(transaction[0]);
        category = transaction[1];
        preprocessed_transactions.append((preprocessed_user_input, category));
    return preprocessed_transactions;



#PREDPROCESIRA TEXT - DOBIMO TABELO OBLIKE (upor_vnos=(beseda1, beseda2,...,besedan) kategorija)
def preprocess_data(file):
    data = open(file, 'r');     #ODPREMO DATOTEKO
    data.readline();            #SPUSTIMO DVE VRSTI                    
    data.readline();
    transactions = get_data_in_table_with_category(data);   #PRIDOBIMO TABELO PODATKOV OBLIKE [(upor. vnos) kategorija]
    data.close();
    transactions = preprocess_transactions_data(transactions);
    return transactions;
    
    
     


#ZAPI�E PREDPROCESIRANE PODATKE V DATOTEKO ZA LEMATIZACIJO
def create_file_for_lemmatization(transactions, file_path):
    file = open(file_path, 'w');
    for transaction in transactions:
        for token in transaction[0]:
            file.write(token.lower());
            file.write(" ");
        file.write(transaction[1]);
        file.write('\n');
    file.close();


#BERE LEMATIZIRANE PODATKE V TABELO
def read_from_file_after_lemmatization(file_path):
    transactions = [];
    file = open(file_path, 'r')
    for line in file:
        tokens = line.split(" ");
        for index in range(len(tokens[:-1])):
            tokens[index] = tokens[index].upper();         
        transaction = (tokens[:-1], tokens[-1].strip());
        transactions.append(transaction);
    return transactions;


#VRNE TABELO TESTNIH PODATKOV. IZ TABELE KI IMA ZA ELEMENTE (upor_vnos= [vnos1,vnos2,...,vnosn],kategorija)
#VRNE RAZBITE VNOSE V OBLIKI (vnos1, kategorija) (vnos2, kategorija) ... 
def get_test_data(transactions):
    test_data = [];
    for transaction in transactions:
        category = transaction[1];
        for input in transaction[0]:
            test_data.append((input, category));
    return test_data;
            








#TESTNA METODA ZA PREDPROCESIRANJE, IZPISUJE VMESNE VREDNOSTI
def preprocess_user_input_TEST(user_input):
    result = [];
    tokens = preprocess.tokenize_tokens(user_input);
    print "TOKENIZE_TOKENS: ",
    print tokens;
    
    tokens = remove_special_characters(tokens);
    print "REMOVE_SPECIAL_CHARACTERS: ",
    print tokens;
    
    print "TOKENS AFTER REMOV SLOV CHAR AND UPPER: ",
    for token in tokens:
        token = replace_slovenian_characters(token);
        token = token.upper();
        print token,
        if is_slovenian_stopword(token):
            continue;
       
        if is_abbreviation(token):                      #GRE ZA KRATICO
            token = token[:-1];                             #ODSTRANIMO PIKO
        oth_abbr = re.split(r'[.;\-:/]+', token);    #RAZBIJEMO NA OSTALE BESEDE PO ZNAKIH
        oth_abbr = replace_all_abbreviations(oth_abbr);  # �E GRE ZA KRATICO, ZAMENJAMO S CELIMI BESEDAMI
        result = result + oth_abbr;       
    
    print ""
    print "TOKENS AFTER REPLACING ABBR: ",
    print result;
    
    result = preprocess.filter_tokens(result, special_chars=True);  #ODSTANIMO POSEBNE ZNAKE
    print "TOKENS AFTER FILTER_TOKENS SPEC CHAR: ",
    print result;
    
    result = preprocess.filter_tokens(result, min_size=2); #ODSTRANIMO ZNAKE DOL�INE 1
    print "TOKENS AFTER FILTET_TOKENS MIN_SIZE=1: ",
    print result;
    
    result = remove_special_characters(result); #ODSTRANIMO OSTALE ZNAKE, KI SE SE POJAVIJO
    print "TOKENS AFTER REMOVE_SPECIAL_CHARACTERS: ",
    print result;
    
    return result;




"""

#NAPOLNIMO SLOVAR KRATIC
abbr_dict = fill_abbreviation_dictionary('pomen_kratic.txt');


#TABELA PREDPROCESIRANIH PODATKOV - BREZ LEMATIZACIHE
transactions = preprocess_data('kategorizirani_podatki.txt');

#IZ TABELE NAREDIMO DATOTEKO ZA LEMATIZIRANJE
create_file_for_lemmatization(transactions,'./Lemmatize/lemmagen/binary/win32/release/before_lematize.txt')

#VRNEMO TABELO LEMATIZIRANIH VNOSOV
lemmatized_transactions = read_from_file_after_lemmatization('./Lemmatize/lemmagen/binary/win32/release/after_lematize.txt')

#VRNEMO TABELO TESTNIH PODATKOV
test_data = get_test_data(lemmatized_transactions);

"""


"""
ZA TESTIRANJE!!!

data = open('kategorizirani_podatki.txt','r');
data.readline();            #SPUSTIMO DVE VRSTI                    
data.readline();
transactions = get_data_in_table_with_category(data);   #PRIDOBIMO TABELO PODATKOV OBLIKE [(upor. vnos) kategorija]
data.close();

print transactions[138];

"""





"""
______________________________________METODE �E ENKRAT______________________________________________

"""



""" 
__________KRATICE_______________

"""
#VRNE POMEN KRATICE, �E JO NAJDE V SLOVARJU, SICER VRNE ORIGINALNO BESEDO
def replace_abbr(abbr, abbr_dict):
    if abbr_dict.has_key(abbr):
        return abbr_dict[abbr];
    else:
        return abbr
    
#SPREJME VNOS RAZBIT NA BESEDE IN ZAMENJA MOREBITNE KRATICE
def replace_all_abbr(tokens, abbr_dict):
    result = [];
    for token in tokens:
        token = replace_abbr(token, abbr_dict);
        result.append(token);
    return result;






"""
________________POSEBNE BESEDE_____________

"""

#VRNE POMEN POSEBNE BESEDE RAZBIZE NA POSAMEZNE BESEDE, �E JO NAJDE V SLOVARJU, SICER VRNE ORIGINALNO BESEDO V TABELI
def replace_special_word_with_meaning(spec_word, spec_words_dict):
    if spec_words_dict.has_key(spec_word):
        return spec_words_dict[spec_word].split("_");
    else:
        return [spec_word];


#SPREJME VNOS RAZBIT NA BESEDE IN ZAMENJA MOREBITNE POSEBNE BESEDE S CELIMI
def replace_all_spec_words(tokens, spec_words_dict):
    result = [];
    for token in tokens:
        result += replace_special_word_with_meaning(token, spec_words_dict);
    return result;






"""
________________________POSEBNI ZNAKI____________

"""

#ZAMENJA POSEBNE ZNAKE Z PRESLEDKOM
def replace_special_characters_with_space(text_input):
    special_char = [":", ";", "/", "-", "."];
    for char in special_char:
        text_input = text_input.replace(char, " ");
    return text_input;

#SPREJME VNOS RAZBIT NA BESEDE IN ZAMENJA POSEBNE ZNAKE S PRESLEDKI (KLI�E ZGORNJO METODO
def replace_spec_char(tokens):
    result = []
    for token in tokens:
        token = replace_special_characters_with_space(token);
        token = token.strip();
        token = token.split(" ");
        result += token;
    return result;





"""
________________________IMENA_______________

"""

#PREVERI, �E JE BESEDA IME - POGLEDA V TABELO IMEN
def replace_name(token, names_dict):
    if token in names_dict:
        return "ABCDEFGH";
    else:
        return token;
    
#SPREJMEN VNOS RAZBIT NA BESEDE IN MOREBITNA IMENA ZAMENJA Z ISTIM STRINGOM
def replace_all_names(tokens, names_dict):
    result = [];
    for token in tokens:
        token = replace_name(token, names_dict)
        result.append(token);
    return result;

#IZ DATOTEKE NAPOLNI TABELO IMEN
def fill_names_from_file(file):
    names_dict = [];
    data = codecs.open(file, encoding="utf-8");
    for line in data.readlines():
        names_dict.append(line.strip());
    data.close();
    return names_dict;













#IZ DATOTEKE PREBERE IN NAPOLNI SLOVAR
def fill_dictionary(file):
    dict = {};
    data = codecs.open(file, encoding="utf-8");
    for line in data.readlines():
        if line.strip() == '':
            continue;
        item = line.split(" ");
        dict[item[0].strip()] = item[1].strip();
    data.close();
    return dict;





"""

__________________________PREDPROCESIRANJE TEXTA_____________________________
"""

#PROCESIRA UPORABNI�KI VNOS
def preprocess_user_input_2(user_input, spec_word_dict, abbr_dict, names_dict):
    user_input = user_input.upper(); #PRETVORIMO VSE V VELIKE �RKE
    user_input = replace_slovenian_characters_2(user_input); #ODSTRANIMO ȩ� JE ZAMENJAMO Z CSZ
    user_input = preprocess.tokenize_tokens(user_input);    #RAZBIJEMO NA BESEDE
    user_input = replace_all_spec_words(user_input, spec_words_dict); #POSEBNE BESEDE ZAMENJAMO S POMENOM
    user_input = replace_spec_char(user_input) #ZAMENJAMO POSEBNE ZNAKE S PRESLEDKI
    user_input = replace_all_spec_words(user_input, spec_words_dict); #POSEBNE BESEDE ZAMENJAMO S POMENOM �E ENKRAT
    user_input = remove_slo_stoppwords(user_input); # ODSTRANIMO NEPOMEMBNE SLOVENSKE BESEDE
    user_input = replace_all_abbr(user_input, abbr_dict); #ZAMENJAMO VSE MOREBITNE KRATICE
    user_input = replace_all_names(user_input, names_dict);
    user_input = preprocess.filter_tokens(user_input, min_size=2, special_chars=True); #ODSTRANIMO POSEBNE ZNAKE IN STRINGE DOL�INE 1
    
    return user_input;

#ODSTRANI NEPOMEMBNE SLOVENSKE BESEDE
def remove_slo_stoppwords(tokens):
    result = [];
    slo_stoppwords = ['IN', 'CE', 'KER', 'DO', 'PRED', 'PO', 'ZA', 'SKOZI', 'KI', 'SE', 'NA', 'PRI'];
    for token in tokens:
        if token in slo_stoppwords:
            continue;
        result.append(token);
    return result;
        

#ZAMENJA �UMNIKE IN SE�NIKE Z NAVADNIMI �RKAMI V NIZU
def replace_slovenian_characters_2(word):
    characters = ['�','�','�','�','�','�','�','�','�','�'];
    word = word.replace('�','C');
    word = word.replace('�','C');
    word = word.replace('�','S');
    word = word.replace('�','S');
    word = word.replace('�','Z');
    word = word.replace('�','Z');
    word = word.replace('�','DJ');
    word = word.replace('�','DJ');
    word = word.replace('�','C');
    word = word.replace('�','C');
    return word;            



#VRNE TABELO PODATKOV V OBLIKI (upor.vnos, kategorija)
def get_data_in_table_with_category_2(data):
    transactions = [];
    for line in data:
        line = line.strip();
        category = line.split(" ")[0];
        input = line.replace(category, '', 1).strip();
        transactions.append((input,category));       
    return transactions;

#PREDPROCESIRA TEXT - DOBIMO TABELO OBLIKE (upor_vnos=(beseda1, beseda2,...,besedan) kategorija)
def preprocess_data_2(file, spec_word_dict, abbr_dict, names_dict):
    data = codecs.open(file, encoding="utf-8");     #ODPREMO DATOTEKO
    data.readline();            #SPUSTIMO DVE VRSTI                    
    data.readline();
    transactions = get_data_in_table_with_category_2(data);   #PRIDOBIMO TABELO PODATKOV OBLIKE [(upor. vnos) kategorija]
    data.close();
    transactions = preprocess_transactions_data_2(transactions, spec_word_dict, abbr_dict, names_dict);
    return transactions;


#ZA VSAKO TRANSAKCIJO KLI�E ZGORNJO METODO, KI PROCESIRA VNOS IN VRNE TABELO VSEH S PRIPADAJO�O KATEGORIJO
def preprocess_transactions_data_2(transactions, spec_word_dict, abbr_dict, names_dict):
    preprocessed_transactions = [];
    for transaction in transactions:
        preprocessed_user_input = preprocess_user_input_2(transaction[0], spec_word_dict, abbr_dict, names_dict);
        category = transaction[1];
        preprocessed_transactions.append((preprocessed_user_input, category));
    return preprocessed_transactions;


#VRNE TABELO TESTNIH PODATKOV. IZ TABELE KI IMA ZA ELEMENTE (upor_vnos= [vnos1,vnos2,...,vnosn],kategorija)
#VRNE RAZBITE VNOSE V OBLIKI (vnos1, kategorija) (vnos2, kategorija) ... 
def get_test_data_2(transactions):
    test_data = [];
    for transaction in transactions:
        category = transaction[1];
        for input in transaction[0]:
            test_data.append((input, category));
    return test_data;



"""
________________________________LEMATIZACIJA________________________________________

"""

#ZAPI�E PREDPROCESIRANE PODATKE V DATOTEKO ZA LEMATIZACIJO
def create_file_for_lemmatization_2(transactions, file_path):
    file = codecs.open(file_path, 'w', encoding="utf-8");
    for transaction in transactions:
        for token in transaction[0]:
            file.write(token.lower());
            file.write(" ");
        file.write(transaction[1]);
        file.write('\n');
    file.close();


#BERE LEMATIZIRANE PODATKE V TABELO
def read_from_file_after_lemmatization_2(file_path):
    transactions = [];
    file = codecs.open(file_path, 'r', encoding="utf-8")
    for line in file:
        tokens = line.split(" ");
        for index in range(len(tokens[:-1])):
            tokens[index] = tokens[index].upper();         
        transaction = (tokens[:-1], tokens[-1].strip());
        transactions.append(transaction);
    return transactions;





"""

text = unicode("PLA�. DELE�A PO POG. 11 12017 2011");


abbr_dict = fill_dictionary('pomen_kratic.txt');
spec_words_dict = fill_dictionary('pomen_posebnih_besed.txt');
names_dict = fill_names_from_file('imena.txt');



print "VNOS:"
print text;
print "";

text = text.upper();
print "PRETVORIMO V VELIKE �RKE:"
print text;
print "";


text = preprocess.tokenize_tokens(text);
print "PREPROCESS.TOKENIZE TOKENS - RAZBIJEMO NA BESEDE"
print text;
print "";

text = replace_all_spec_words(text, spec_words_dict);
print "REPLACE_ALL_SPEC_WORDS - POSEBNE BESEDE ZAMENJAMO S POMENOM"
print text;
print "";

text = replace_spec_char(text);
print "REPLACE_SPEC_CHAR - ZAMENJAMO POSEBNE ZNAKE S PRESLEDKI"
print text;
print "";

#KLI�EMO �E ENKRAT, V PRIMERU DA NAJDE POSEBNO BESEDO KO ODSRANIMO POSEBNE ZNAKE
text = replace_all_spec_words(text, spec_words_dict);
print "REPLACE_ALL_SPEC_WORDS - POSEBNE BESEDE ZAMENJAMO S POMENOM �E ENKRAT"
print text;
print "";


text = remove_slo_stoppwords(text);
print "REMOVE_SLO_STOPPWORDS - ODTRANIMO NEPOMEMBNE SLOVENSKE BESEDE "
print text;
print "";


text = replace_all_abbr(text, abbr_dict);
print "REPLACE_ALL_ABBR - KRATICE ZAMENJAMO Z POMENOM"
print text;
print "";

text = replace_all_names(text, names_dict);
print "REPLACE ALL NAMES - ODSTRANIMO VSA IMENA"
print text;
print "";

text = preprocess.filter_tokens(text, min_size=2, special_chars=True);
print "PREPROCESS.FILTER_TOKENS: OSTRANIMO POSEBNE VNOSE IN STRINGE VEL. 1"
print text;
print "";

"""












#NAPOLNIMO SLOVAR
abbr_dict = fill_dictionary('pomen_kratic.txt');
spec_words_dict = fill_dictionary('pomen_posebnih_besed.txt');
names_dict = fill_names_from_file('imena.txt');


#PRIDOBIMO TRANSAKCIJE
transactions = preprocess_data_2('kategorizirani_podatki_2.txt', spec_words_dict, abbr_dict, names_dict);


#PREPI�EMO PODATKE V DATOTEKO ZA LEMATIZACIJO
create_file_for_lemmatization_2(transactions, './Lemmatize/lemmagen/binary/win32/release/before_lematize.txt');

#PO LEMATIZACIJI PODATKE VRNEMO NAZAJ V TABELO
lemmatized_transactions = read_from_file_after_lemmatization_2('./Lemmatize/lemmagen/binary/win32/release/after_lematize.txt');

#VRNEMO TABELO TESTNIH PODATKOV
test_data = get_test_data_2(lemmatized_transactions);
