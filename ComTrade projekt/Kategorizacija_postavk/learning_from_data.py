#!/usr/bin/python
# -*- coding: iso-8859-2 -*-
from __future__ import division
'''
Created on 24. dec. 2013

@author: Igor Klepić
'''



"""
TEAVE:
    -lematizacije - plačilo, nakazilo (utf encoding, dodaj pravilo za nakazilo)
    -izbris čudnih vnosov - prazen vnos - posebna kategorija
    -odstanitev posebnih znakov - vnos ppstane neregularen npr. (RACUN-3332) ko odstranimo - nadomestimo s presledkom
    -kratice z eno črko - odstrani npr, D.O.O., D.D.
    - zamenjava č - unidecode knjinica
    
    -posebni izrazi npr. BS -slovar
    -samo tevilke brez pik pustimo, zamenjamo zz skupno besedo
    -odstrani imena ali pa vsa vsa imena zamenjaj z enakim nizom - knjinica imen DONE!
    -open file - z razredom codecs
    
    
    TODO: 18.12.2013:
        -lematizacija, ponovno se naučiti
        -spraviti v obliko (vnos, kategorija)
        -prazne vnose posebna kategorija
        -napolni seznam posebnih vnosov npr d.o.o., bs
        -unidecode uporabi povsod
        -napolni podatke z imeni
        -č je popravi, tudi v slovarju
    
"""

# -*- coding: utf8 -*-
import preprocess;
import re;
from unidecode import unidecode;
import codecs;
import nltk;
import numpy as np;
from sklearn.naive_bayes import MultinomialNB;
from sklearn.feature_extraction.text import CountVectorizer
import random;


from preprocessing_data import preprocess_user_input_2;
from preprocessing_data import fill_dictionary;
from preprocessing_data import fill_names_from_file;






#BERE LEMATIZIRANE PODATKE V TABELO
def read_from_file_after_lemmatization_2(file_path):
    transactions = [];
    file = codecs.open(file_path, 'r', encoding="utf-8")
    for line in file:
        tokens = line.split(" ");
        for index in range(len(tokens[:-1])):
            tokens[index] = tokens[index].upper();         
        transaction = (tokens[:-1], tokens[-1].strip());
        transactions.append(transaction);
    return transactions;


def create_file_for_lemmatization_2(transactions, file_path):
    file = codecs.open(file_path, 'w', encoding="utf-8");
    for transaction in transactions:
        for token in transaction[0]:
            file.write(token.lower());
            file.write(" ");
        file.write(transaction[1]);
        file.write('\n');
    file.close();



#VRNE TABELO TESTNIH PODATKOV. IZ TABELE KI IMA ZA ELEMENTE (upor_vnos= [vnos1,vnos2,...,vnosn],kategorija)
#VRNE RAZBITE VNOSE V OBLIKI (vnos1, kategorija) (vnos2, kategorija) ... 
def get_test_data_2(transactions):
    test_data = [];
    for transaction in transactions:
        category = transaction[1];
        for input in transaction[0]:
            test_data.append((input, category));
    return test_data;


#IZ TABELE TRANSAKCIJ PO PROCESIRANJU ODSTRANI TISTE, KI SO PRAZNE
def remove_empty_ones(transactions):
    result = [];
    for item in transactions:
        if len(item[0])==0:
            continue;
        result.append(item);
    return result;








def return_word(word):
    return {'beseda': word};




#PO LEMATIZACIJI PODATKE VRNEMO NAZAJ V TABELO
lemmatized_transactions = read_from_file_after_lemmatization_2('./Lemmatize/lemmagen/binary/win32/release/before_lematize.txt');

#print len(lemmatized_transactions);



#ODSTRANIMO MOREBITNE PRAZNE VNOSE (NEVELJAVNE)
lemmatized_transactions = remove_empty_ones(lemmatized_transactions);

#create_file_for_lemmatization_2(lemmatized_transactions, './Lemmatize/lemmagen/binary/win32/release/after_removing_empty.txt')

#print len(lemmatized_transactions);

#VRNEMO TABELO TESTNIH PODATKOV
#test_data = get_test_data_2(lemmatized_transactions);

random.shuffle(lemmatized_transactions);

vectorizer = CountVectorizer(min_df=1)
X = vectorizer.fit_transform([" ".join(item[0]) for item in lemmatized_transactions])
y = [item[1] for item in lemmatized_transactions]



train_set_X = X[10000:]
train_set_y = y[10000:]

test_set_X = X[:10000]
test_set_y = y[:10000]


clf = MultinomialNB();

clf.fit(train_set_X, train_set_y)




#print test_set_X[100].toarray();

correctly = 0;
index = 0;
for item in test_set_X:
    predicted_category = clf.predict(item)[0];
    if predicted_category == test_set_y[index]:
        correctly += 1;
    index += 1;


print correctly/index;


"""
                                                
item0 = " ".join(lemmatized_transactions[24][0]);
item1 = " ".join(lemmatized_transactions[25][0]);
item2 = " ".join(lemmatized_transactions[27][0]);

print item0;
print item1;
print item2;
vectors = vectorizer.fit_transform([item0, item1, item2]);
print vectors.toarray();
"""






"""
_________________________________TESTING CLASSIFICATION FOR SOME INPUT___________________________________

"""

"""
abbr_dict = fill_dictionary('pomen_kratic.txt');
spec_words_dict = fill_dictionary('pomen_posebnih_besed.txt');
names_dict = fill_names_from_file('imena.txt');


test_input = unicode("TUS SM BTC LJUBLJANA");
test_input = preprocess_user_input_2(test_input, spec_words_dict, abbr_dict, names_dict);
test_input = " ".join(test_input)


for index in range(len(lemmatized_transactions)):
    if test_input in " ".join(lemmatized_transactions[index][0]):
        print clf.predict(X[index]);
"""







#test_input = unicode("VOKA-VODA IN KANAL. SI120001238850847");
#test_input = preprocess_user_input_2(test_input, spec_words_dict, abbr_dict, names_dict);
#print test_input
#print " ".join(test_input)
#vector = vectorizer.fit_transform([" ".join(test_input)]).toarray();
#print vector[0];
#vector = vectorizer.fit_transform([" ".join(test_input)]);
#print clf.predict(vector);















#for index in range(len(X)):
#    vectorizer.fit_transform(X[index]).toarray();
#    print index;



