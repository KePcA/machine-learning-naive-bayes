'''
Created on 26. nov. 2013

@author: Pavilion
'''
import preprocess;
from textblob import TextBlob;
import re;



#METODE, KI PREPOZNA KRATICO, ZRAVEN DODA SE VNOS IZ KATERE JE KRATICO PREJELA
def get_abbreviations(transactions):
    abbreviations_dictionary = [];
    for transaction in transactions:
        input = transaction[0];
        tokens = preprocess.tokenize_tokens(input);
        for token in tokens:
            if (token[-1] is '.'):
                abbreviations_dictionary.append((token, input));
                
    return abbreviations_dictionary;

#ZA DOLOCENO KRATICO VRNE VSE RAZLICNE VNOSE, KJER SE POJAVI
def get_all_inputs_from_abbreviation(abbreviation,dictionary):
    inputs_list = []
    for item in dictionary:
        if (abbreviation == item[0]):
            input = item[1];
            if (input not in inputs_list):
                inputs_list.append(input)
    
    return inputs_list;
                
#VRNE VSE KRATICE, KI SE POJAVIJO
def get_all_unique_abbreviations(dictionary):
    abbreviations = []
    for item in dictionary:
        abbreviation = item[0];
        if (abbreviation not in abbreviations):
            abbreviations.append(abbreviation)
    
    return abbreviations;

#PREVERI, ALI OBSTAJA KRATICA, KI PRE
        

#PREPOZNA KRATICO IN ODSRANI PIKO NA KONCU. V PRIMERU, DA SKUPAJ VEc KRATIC, KER MED SABO NI PRESLEDKA,       
#POTEM RAZBIJE VNOS NA VSAKO KRATICO POSEBEJ BREZ PIKE
def get_abbreviations_SECOND(transactions):
    abbreviations_dictionary = [];
    for transaction in transactions:
        input = transaction[0];
        tokens = preprocess.tokenize_tokens(input);
        for token in tokens:
            if (token[-1] is '.'):
                token = preprocess.tokenize_tokens(token);  #Odstrani piko na koncu
                other_abbr = re.split(r'[.;\-:]+', token[0]); #Razbije na vse ostalo
                for abbr in other_abbr:
                    abbreviations_dictionary.append((abbr, input));
                
    return abbreviations_dictionary;









data = open('kategorizirani_podatki.txt', 'r')


'''
for line in data:
    print line
'''
transactions = []
data.readline()
data.readline()


#Napolni tabelo transactions oblike [(upor.vnos),(kategorija)]
for line in data:
    line = line.strip()
    category = line.split(" ")[0]
    input = line.replace(category, '', 1).strip()
    transactions.append((input,category))
    
data.close()    


   
abbreviations_dictionary = get_abbreviations(transactions);
abbreviations_dictionary_SECOND = get_abbreviations_SECOND(transactions);

print abbreviations_dictionary_SECOND[1]



""" 
print abbreviations_dictionary[2];
"""

"""
inputs_list = get_all_inputs_from_abbreviation("JUL.",abbreviations_dictionary);
for input in inputs_list:
    print input;
"""

inputs_list = get_all_inputs_from_abbreviation("IZB",abbreviations_dictionary_SECOND);
for input in inputs_list:
    print input;
    


all_abbreviations = get_all_unique_abbreviations(abbreviations_dictionary); 
all_abbreviations_SECOND = get_all_unique_abbreviations(abbreviations_dictionary_SECOND); 

output = open('all_abbr.txt','w');
for abbr in all_abbreviations_SECOND:
    output.write(abbr)
    output.write('\n');


""" 
print len(all_abbreviations);
print all_abbreviations[0:20];

inputs_list = get_all_inputs_from_abbreviation("EN.",abbreviations_dictionary);
for input in inputs_list:
    print input;
"""    


#VARIANTA KRATIC IN POJAV S PIKAMI IN BREZ RAZBITJA
output = open('input_abbr.txt','w')
    
for abbreviation in all_abbreviations:
    output.write(abbreviation); #write abbreviation into file
    output.write('\n');
    inputs_list = get_all_inputs_from_abbreviation(abbreviation,abbreviations_dictionary);
    for input in inputs_list:
        output.write(input); #write input for current abbreviation into file
        output.write('\n');
    
    output.write('\n');  #write blank line at the end of current abbreviation

output.close()



#VARIANTA KRATIC IN POJAV BREZ PIK Z RAZBITJEM
output = open('input_abbr_SECOND.txt','w')
    
for abbreviation in all_abbreviations_SECOND:
    output.write(abbreviation); #write abbreviation into file
    output.write('\n');
    inputs_list = get_all_inputs_from_abbreviation(abbreviation,abbreviations_dictionary_SECOND);
    for input in inputs_list:
        output.write(input); #write input for current abbreviation into file
        output.write('\n');
    
    output.write('\n');  #write blank line at the end of current abbreviation

output.close()








