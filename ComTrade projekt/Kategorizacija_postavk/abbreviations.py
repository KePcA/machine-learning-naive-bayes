'''
Created on 5. dec. 2013

TABELA NAJPOGOSTEJ�IH KRATIC

@author: Pavilion
'''
"""
TISTI, KI SMO JIH IZPUSTILI:
CO. , R. , FIN. , (Z.O.O.) , S. , NAL. , POL. , LET. ,
KUR. , NAPR. , D. , BOH. , L. , ZAPADL. , V. , K. , LEK. , 
IZD. , PO. , OPR. , KRAT. , TELEK. , DIJ. , UPR. , TEK. , 
BK. , JULI. , CUST. , NO. , OKR. , ODD. , SI. , ODPL. , 
REG. , MES. , DOP. , P. , POSTOP. , OS. , SK. (SKLAD) , �
MERC. , SPEC. , STAR. , VARS. , DOD.(DODIPLOMSKI) , IND. , 
REFUND. , PREK., ORG. , DIGIT. , POT. ??? , ZAVAROVAL. , 
UL. , OBVEZN. , ODST. , POVRAC., KOL. , KO. , JUN. , TOPLO. , 
NAKAZILOPL. , ZELEZ , GR. , NADOM. , FLEX. , POSAMEZ. , IZVED. , 
PLA. , AMBUL. , SPAGET. , SL. , SLOV., PREH. , FINA. , DRAG. , 
ZADR. , OTR. , PROST. , ODB. , POSKOD. , OB. , POSTOJN. , 
TELEKOMUN. , DO. , ZAHT. , POTOV. , SV. , BLAG. , RN. , 
PON. , �IVLJEN. , PREDRN. , TEHN. , OBRA�. , STORI. , KABEL. , 
ZDRAVS. , AKON. , RAV. , STVB. , DOKUM. , NAP. , IZB. , ROM. , 
POST. , KUPOPROD. , VAR�E. , NOTR. , UK. , MOB. , TEL. , OBRAB., 
U�B. , GIM. 
"""

abbr_dict = {'JUL.':"JULIJ"};
abbr_dict['JUL'] = "JULIJ"
abbr_dict['PL'] = "PLACILO"
abbr_dict['PLA�'] = "PLACILO";
abbr_dict['PLAC'] = "PLACILO";
abbr_dict['ELEKTR'] = "ELEKTRICNA"
abbr_dict['EL'] = "ELEKTRIcNA";
abbr_dict['ELEK'] = "ELEKTRICNA";
abbr_dict['ELEKT'] = "ELEKTRICNA";
abbr_dict['EN'] = "ENERGIJA"
abbr_dict['ENERG'] = "ENERGIJA";
abbr_dict['ENE'] = "ENERGIJA"
abbr_dict['ENER'] = "ENERGIJA"
abbr_dict['OBV'] = "OBVEZNOST"
abbr_dict['OBVEZ'] = "OBVEZNOST"
abbr_dict['ZAP'] = "ZAPADLOST";
abbr_dict['ZAV'] = "ZAVAROVANJE";
abbr_dict['�T'] = "STEVILKA";
abbr_dict['ST'] = "STEVILKA";
abbr_dict['NEG'] = "NEGATIVEN";
abbr_dict['STR'] = "STROSEK";
abbr_dict['RA�'] = "RACUN";
abbr_dict['RAC'] = "RACUN";
abbr_dict['R�'] = "RACUN";
abbr_dict['AKONT'] = "AKONTACIJA";
abbr_dict['DOHOD'] = "DOHODNINA";
abbr_dict['LJ'] = "LJUBLJANA";
abbr_dict['ZDR'] = "ZDRAVSTVENI";
abbr_dict['ZDRAVST'] = "ZDRAVSTVENI";
abbr_dict['�IV'] = "ZIVLJENSKI";
abbr_dict['KANAL'] = "KANALIZACIJA";
abbr_dict['KAN'] = "KANALIZACIJA";
abbr_dict['PREM'] = "PREMOZENJSKO";
abbr_dict['KOM'] = "KOMUNALA";
abbr_dict['REZ'] = "REZERVNI";
abbr_dict['KART'] = "KARTICA";
abbr_dict['�L'] = "CLANARINA";
abbr_dict['STAN'] = "STANOVANJE";
abbr_dict['VOD'] = "VODOVOD";
abbr_dict['STORIT'] = "STORITEV";
abbr_dict['STOR'] = "STORITEV";
abbr_dict['STO'] = "STORITEV";
abbr_dict['POG'] = "POGODBA";
abbr_dict['RAVN'] = "RAVNANJE";
abbr_dict['ODP'] = "ODPADEK";
abbr_dict['PR'] = "PREMIJA";
abbr_dict['REST'] = "RESTAVRACIJA";
abbr_dict['RESTAVR.'] = "RESTAVRACIJA";
abbr_dict['SKL'] = "SKLAD";
abbr_dict['PROD'] = "PRODAJALNA";
abbr_dict['NAJEMN'] = "NAJEMNINA";
abbr_dict['MAT'] = "MATERIJALNI";
abbr_dict['PRISPEV'] = "PRISPEVEK";
abbr_dict['PRISP'] = "PRISPEVEK";
abbr_dict['POSL'] = "POSLOVALNICA";
abbr_dict['TOP'] = "TOPLARNA";
abbr_dict['OBR'] = "OBRACUN";
abbr_dict['AVG'] = "AVGUST";
abbr_dict['NAR'] = "NAROCNINA";
abbr_dict['NARO�'] = "NAROCNINA";
abbr_dict['OSKR'] = "OSKRBA";
abbr_dict['PREDR.'] = "PREDRACUN";
abbr_dict['PREDRA�'] = "PREDRACUN";
abbr_dict['PREDRAC'] = "PREDRACUN";
abbr_dict['POS'] = "POSOJILO";
abbr_dict['TRG'] = "TRGOVINA";
abbr_dict['VAR�'] = "VARCEVANJE";
abbr_dict['ZAVAR'] = "ZAVAROVANJE";
abbr_dict['ZAVAROV'] = "ZAVAROVANJE";
abbr_dict['KOMUNAL'] = "KOMUNALNI";
abbr_dict['ZEMLJ'] = "ZEMLJISCE";
abbr_dict['PONUD'] = "PONUDBA";
abbr_dict[''] = "";
abbr_dict[''] = "";





