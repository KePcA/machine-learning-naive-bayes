#!/usr/bin/python
# -*- coding: iso-8859-2 -*-

'''
Created on 25. dec. 2013

ZA ANALIZO ZA�ETNIH PODATKOV

@author: Pavilion
'''



from preprocessing_data import get_data_in_table_with_category_2;
import codecs;

#ZA NEK VNOS (input) POGLEDA, KOLIKOKRAT SE POJAVI V KAK�NI OD KATEGORIJ V PODATKIH (data)
def different_category_for_same_input(input, data):
    counter_dict = {};
    for item in data:
        if input == item[0]:
            category = item[1];
            if counter_dict.has_key(category):
                counter_dict[category] += 1;
            else:
                counter_dict[category] = 1;
    return counter_dict;


def different_category_for_same_input_substring(input, data):
    counter_dict = {};
    for item in data:
        if input in item[0]:
            category = item[1];
            if counter_dict.has_key(category):
                counter_dict[category] += 1;
            else:
                counter_dict[category] = 1;
    return counter_dict;


def print_counter_dict(counter_dict, input):
    print "PRINTING COUNTING FOR INPUT: "+input;
    print "";
    for key, value in counter_dict.iteritems():
        print key + ":  " + str(value);



data = codecs.open('kategorizirani_podatki_2.txt', encoding="utf-8");     #ODPREMO DATOTEKO
data.readline();            #SPUSTIMO DVE VRSTI                    
data.readline();
initial_data = get_data_in_table_with_category_2(data);   #PRIDOBIMO TABELO PODATKOV OBLIKE [(upor. vnos) kategorija]
data.close();



"""
user_input = "BANKOMAT DVIG GOT DOMA";
counter_dict = different_category_for_same_input(user_input, initial_data);

print_counter_dict(counter_dict, user_input);
#print counter_dict;
"""




input_examples = ["BANKOMAT DVIG GOT DOMA", "PROVIZIJA", "NADOMESTILO DB"]

output = codecs.open('kategorizacija_nekaterih_vnosov.txt', 'w', encoding="utf-8");

for input in input_examples:
    output.write(input)
    output.write('\n');
    counter_dict = different_category_for_same_input(input,initial_data);
    for key, value in counter_dict.iteritems():
        output.write(key)
        if key == "1002": output.write(":\t");
        else: output.write(":\t\t");
        output.write(str(value));
        output.write('\n');
    output.write('\n');


output.close();



