'''
Created on 12. nov. 2013

@author: Pavilion
'''



from nltk.corpus import names
import nltk
import random

#Funkcija, ki vrne zadnjo crko imena
def gender_features(word):
    return {'last_letter': word[-1]}

#print gender_identification.gender_features('Shrek')

#Pripravimo seznam imen in pripadajoce razrede
names = ([(name, 'male') for name in names.words('male.txt')] + [(name, 'female') for name in names.words('female.txt')])
random.shuffle(names)

#Vzamemo priljubljeni ekstraktor za procesiranje imen
#Razdelimo na TRAINING SET in TEST SET
featuresets = [(gender_features(n), g) for (n,g) in names]
train_set, test_set = featuresets[500:], featuresets[:500]
classifier = nltk.NaiveBayesClassifier.train(train_set)
print nltk.classify.accuracy(classifier, test_set)



#Testiramo nekaj primerov imen
#classifier.classify(gender_features('Neo'))
#classifier.classify(gender_features('Trinity'))
#print nltk.classify.accuracy(classifier, test_set)

#Raziscemo, katere lastosti so najbolj znacilne za kak spol - LIKEHOOD RATIONS
#classifier.show_most_informative_features(5)


    
    
    
    
    