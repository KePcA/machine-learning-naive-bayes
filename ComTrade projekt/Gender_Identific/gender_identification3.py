'''
Created on 14. nov. 2013

@author: Pavilion
'''

from nltk.corpus import names
import nltk
import random


#Uporabimo se dve zadnji crki imena
def gender_features3(word):
    return {'suffix1': word[-1:],
            'suffix2': word[-2:]}
    


names = ([(name, 'male') for name in names.words('male.txt')] + [(name, 'female') for name in names.words('female.txt')])
random.shuffle(names)


#Locimo na development set in test set. V developmentu imamo train set in dev-test za napake
train_names = names[1500:]
devtest_names = names[500:1500]
test_names = names[:500]


train_set = [(gender_features3(n), g) for (n,g) in train_names]
devtest_set = [(gender_features3(n), g) for (n,g) in devtest_names]
test_set = [(gender_features3(n), g) for (n,g) in test_names]
classifier = nltk.NaiveBayesClassifier.train(train_set) #Treniramo na train set
print nltk.classify.accuracy(classifier, devtest_set) #Pozenemo na devtestu



    
