'''
Created on 14. nov. 2013

@author: Pavilion
'''

from nltk.corpus import names
import nltk
import random


#Druga funkcija, kjer uporabimo vse kar se spomnimo, vse crke
def gender_features2(name):
    features = {}
    features["firstletter"] = name[0].lower() #PRVA CRKA
    features["lastletter"] = name[-1].lower() #ZADNJA CRKA
    for letter in 'abcdefghijklmnopqrstuvwxyz':
        features["count(%s)" % letter] = name.lower().count(letter)
        features["has(%s)" % letter] = (letter in name.lower())
    return features


names = ([(name, 'male') for name in names.words('male.txt')] + [(name, 'female') for name in names.words('female.txt')])
random.shuffle(names)



featuresets = [(gender_features2(n), g) for (n,g) in names]
train_set = featuresets[500:]
test_set = featuresets[:500]
classifier = nltk.NaiveBayesClassifier.train(train_set)
print nltk.classify.accuracy(classifier, test_set)

